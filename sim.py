import math
import numpy as np
import scipy as sp
from copy import deepcopy

DT = 0.001
DY = 1
FLOOR = -10
g = (0, -5) # gravitational acceleration
FR = 0.05 # friction rate
RADIUS = 0.001

def norm(vector) :
    total = 0
    for component in vector :
        total += component ** 2
    return math.sqrt(total)

def dist(a, b) :
    return math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)

class FlowData :
    '''
    At a specific time
    Force vectors
    Frictions
    '''
    def __init__(self, time, cnt, particles) :
        self.time = time
        self.cnt = cnt
        self.particles = particles

    def show(self) :
        data = ""
        for particle in self.particles :
            pos = str(particle.pos)
            force = str(particle.force)
            friction = str(particle.friction)
            data += pos + ", " + force + "," + friction
            data += "\n"
        return data


class World :
    '''
    - properties
        flow : list of particles
        tube : a function of y that represents the boundary of a tube
        h : the height of the tube
    '''

    def __init__(self) :
        self.time = 0
        self.cnt = 0

        self.flow = []
        self.tube = None
        self.tube_friction = 0
        self.h = 0

        # list of FlowData
        self.data = []

    def define_height(self, h) :
        '''
            defines the height of tube
        '''
        self.h = h

    def define_tube(self, tube, tf) :
        '''
            defines the shape and properties of tube
        '''
        self.tube = tube
        self.tube_friction = tf
    
    def gen_particles(self) :
        '''
            generate particles above the tube
            to pour down through the tube
        '''
        uid = 0
        #for y in range(1, 10) :
        #    for r in range(-3, 4) :
        #        self.flow.append(Particle((r, y + self.h), uid))
        #        uid += 1
        r = -3
        y = 1
        print("Generating Particles...")
        while y <= 10 :
            while r <= 3 :
                pos = (r, y + self.h)
                self.flow.append(Particle(pos, uid))

                r += RADIUS * 2
                uid += 1
            y += RADIUS * 2


    
    def show_particles(self) :
        for i, particle in enumerate(self.flow) :
            print("The particle {} : pos - ({:2f},{:2f}), vel - ({:2f},{:2f})".format(i, particle.pos[0], particle.pos[1], particle.velocity[0], particle.velocity[1]))

    def num_particles(self) :
        return len(self.flow)

    def update_world(self) :
        '''
        1. solve EOM
        2. update
        '''
        for p in self.flow :
            left = False
            # At a specific height, the value of r is determined
            r = self.tube(p.pos[1])
            # if the particle is on the left side
            if p.pos[0] < 0 :
                r = -r
                left = True
            # particle - tube contact
            if abs(r - p.pos[0]) < p.radius * 2 :
                # funnel hole
                if p.pos[1] < 0.2 :
                    # no normal force
                    # fall down
                    pass
                else :
                    # normal force exerted
                    # calculate normal force
                    m = p.mass
                    Fr = -m * norm(g) * 3 / 4
                    Fy = m * norm(g) * np.sqrt(3) / 4
                    if left :
                        Fr = -Fr
                    normal_force = (Fr, Fy)
                    #print("Normal Force! :", normal_force)
                    p.add_force(normal_force)
                    # friction
                    p.dissipative(norm(normal_force))
            

            # particle - particle contact
            for _p in self.flow :
                if p.uid == _p.uid :
                    continue
                
                d = dist(p.pos, _p.pos)
                if d < RADIUS * 2 :
                    # normal force
                    resistance = (1.0 - d)
                    # direction : other particle to the particle
                    direction = (p.pos[0] - _p.pos[0], p.pos[1] - _p.pos[1])
                    Fr = resistance * direction[0]
                    Fy = 4 * resistance * direction[1]
                    resist_force = (Fr, Fy)
                    p.add_force(resist_force)
                    p.dissipative(resistance)
        
        # gravitational field
        # update positions
        self.time += DT
        self.cnt += 1
        save = deepcopy(self.flow)
        for p in self.flow :
            p.add_force(g)
            p.update_pos()
        # save data
        self.data.append(FlowData(self.time, self.cnt, save))

class Particle :
    '''
        radius = 0.5
        mass = 1.0
        pos = (r, y) : tuple
    '''
    #self.fric_pp = 1.0 # the coeff of friction between two particles
    #self.fric_pt = 0.5 # the coeff of friction between a particle and the tube
    def __init__(self, pos : tuple, uid) :
        self.radius = RADIUS
        self.mass = 1.0
        self.pos = pos
        self.velocity = (0, 0); # (dr/dt, dy/dt)
        self.friction = 0 # magnitube
        self.force = (0, 0)
        
        # unique identical number to distinguish a particle from others
        self.uid = uid

    def update_pos(self) :
        dr = self.force[0] / self.mass * DT
        dy = self.force[1] / self.mass * DT
        self.friction *= FR
        drf = -self.friction
        dyf = -self.friction
        if dr > 0 :
            drf = -drf
        new_pos = [self.pos[0] + dr + drf, self.pos[1] + dy + dyf]
        if new_pos[1] < -10 :
            new_pos[1] = -10
        self.pos = tuple(new_pos)
        self.velocity = (dr / DT, dy / DT)

        # clear
        self.force = (0, 0)
        self.friction = 0
    
    def show_pos(self) :
        print("({}, {})".format(self.pos[0], self.pos[1]))
    
    def get_E(self) :
        kinetic = 0.5 * self.mass * norm(self.velocity) ** 2
        potential = self.mass * self.pos[1] * g
        return kinetic + potential
    
    def add_force(self, force) :
        self.force = self.force[0] + force[0], self.force[1] + force[1]

    def dissipative(self, f = 0.5) :
        self.friction += f